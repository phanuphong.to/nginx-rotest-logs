#!/bin/sh

LOGS_PATH=/var/log/nginx
SAVE_DAYS=7
TODAY=$(date +"%Y-%m-%d-%H")
CURRENTTIME=$(date +"%Y-%m-%d %H-%M-%S")

if [ -f ${LOGS_PATH}/error_${TODAY}.log.gz ];then
TODAY=$CURRENTTIME
fi
if [ -f ${LOGS_PATH}/access_${TODAY}.log.gz ];then
TODAY=$CURRENTTIME
fi

#Backup log files
mv ${LOGS_PATH}/error.log ${LOGS_PATH}/error_${TODAY}.log
mv ${LOGS_PATH}/access.log ${LOGS_PATH}/access_${TODAY}.log

#Reload nginx
nginx -s reload

#Compress xxx.log to xxx.log.gz and auto remove xxx.log
if [ -f "/usr/bin/gzip" ];then
gzip ${LOGS_PATH}/error_${TODAY}.log
gzip ${LOGS_PATH}/access_${TODAY}.log
fi

find ${LOGS_PATH} -type f -mtime +${SAVE_DAYS} | xargs rm -f
exit 0