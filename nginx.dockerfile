FROM nginx:stable-alpine

#copy config script to file path /etc/crontabs/script/ in image file
COPY ./script/clearlog.sh /etc/crontabs/script/clearlog.sh
COPY ./script/start.sh /etc/crontabs/script/start.sh
COPY ./script/crontab-config /etc/crontabs/root

#set permission 0744 to file clearlog.sh and start.sh
RUN chmod 0744 /etc/crontabs/script/clearlog.sh
RUN chmod 0744 /etc/crontabs/script/start.sh

#CMD ["nginx", "-g", "daemon off;"]

#run file script /etc/crontabs/script/start.sh 
CMD ["/etc/crontabs/script/start.sh"]


